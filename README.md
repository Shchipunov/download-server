# Download server #

It's a basis for implementation of a download server.

### Project description ###
The project demonstrates basic principles such as:

* GET and HEAD requests
* If None Match / If Modified Since headers
* Bandwidth per connection

The project uses:

* Spring Boot
* Spring (MVC)
* Guava