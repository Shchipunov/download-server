package com.shchipunov.component;

import java.io.File;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Presents an implementation of {@link ResourceStorage}
 * 
 * @author Shchipunov Stanislav
 */
@Component
public class FileStorageImplementation implements ResourceStorage {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileStorageImplementation.class);

	/**
	 * Finds a resource
	 */
	@Override
	public Optional<ResourceWrapper> findOne(String rootFolder, String pathToResource) {
		LOGGER.debug("Root folder: {}", rootFolder);
		LOGGER.debug("Path to resource: {}", pathToResource);

		ResourceWrapper resourceWrapper = new FileWrapperImplementation(new File(rootFolder, pathToResource));
		return Optional.of(resourceWrapper);
	}
}
