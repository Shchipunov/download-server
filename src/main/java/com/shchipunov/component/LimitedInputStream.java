package com.shchipunov.component;

import com.google.common.util.concurrent.RateLimiter;

import java.io.IOException;
import java.io.InputStream;

/**
 * Presents a decorator for {@link InputStream}
 * 
 * @author Shchipunov Stanislav
 */
public class LimitedInputStream extends InputStream {

	private final InputStream inputStream;
	private final RateLimiter rateLimiter;

	public LimitedInputStream(InputStream inputStream, RateLimiter rateLimiter) {
		this.inputStream = inputStream;
		this.rateLimiter = rateLimiter;
	}

	@Override
	public int read() throws IOException {
		rateLimiter.acquire();
		return inputStream.read();
	}

	@Override
	public int read(byte[] buffer) throws IOException {
		rateLimiter.acquire(buffer.length);
		return inputStream.read(buffer);
	}

	@Override
	public int read(byte[] buffer, int offset, int length) throws IOException {
		rateLimiter.acquire(length);
		return inputStream.read(buffer, offset, length);
	}

	@Override
	public synchronized void reset() throws IOException {
		inputStream.reset();
	}

	@Override
	public long skip(long numberOfBytes) throws IOException {
		return inputStream.skip(numberOfBytes);
	}

	@Override
	public int available() throws IOException {
		return inputStream.available();
	}

	@Override
	public void close() throws IOException {
		inputStream.close();
	}

	@Override
	public synchronized void mark(int readLimit) {
		inputStream.mark(readLimit);
	}

	@Override
	public boolean markSupported() {
		return inputStream.markSupported();
	}

}