package com.shchipunov.component;

import java.util.Optional;

/**
 * Presents an interface of the storage abstraction
 * 
 * @author Shchipunov Stanislav
 */
public interface ResourceStorage {

	Optional<ResourceWrapper> findOne(String rootFolder, String pathToResource);

}