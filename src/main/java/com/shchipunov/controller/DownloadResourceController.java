package com.shchipunov.controller;

import static org.springframework.http.HttpHeaders.IF_MODIFIED_SINCE;
import static org.springframework.http.HttpHeaders.IF_NONE_MATCH;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.HEAD;

import java.io.File;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.util.concurrent.RateLimiter;
import com.shchipunov.component.LimitedInputStream;
import com.shchipunov.component.ResourceStorage;
import com.shchipunov.component.ResourceWrapper;

/**
 * Controller that allows to download resources
 * 
 * @author Shchipunov Stanislav
 */
@RestController
public class DownloadResourceController {

	private static final Logger LOGGER = LoggerFactory.getLogger(DownloadResourceController.class);

	private static final double BANDWIDTH_PER_CONNECTION = 1048576; // 1 MB
	private static final String DEFAULT_ROOT_FOLDER = new File(File.separator).getAbsolutePath();

	private final ResourceStorage resourceStorage;

	@Autowired
	public DownloadResourceController(ResourceStorage resourceStorage) {
		this.resourceStorage = Objects.requireNonNull(resourceStorage);
	}

	/**
	 * Allows to download a resource for the given URL
	 * 
	 * @param date
	 * @param uuid
	 * @param resourceName
	 * @param httpMethod
	 * @param ifNoneMatchHeader
	 * @param ifModifiedSinceHeader
	 * @return
	 */
	@RequestMapping(method = { GET, HEAD }, value = "/download/{date}/{uuid}/{resourceName:.+}")
	public ResponseEntity<Resource> downloadResource(
			@PathVariable String date, 
			@PathVariable String uuid,
			@PathVariable String resourceName, 
			HttpMethod httpMethod, 
			@RequestHeader(IF_NONE_MATCH) Optional<String> ifNoneMatchHeader,
			@RequestHeader(IF_MODIFIED_SINCE) Optional<Date> ifModifiedSinceHeader) {
		LOGGER.debug("Starts to process a request...");
		Optional<ResourceWrapper> resourceWrapperOptional = resourceStorage.findOne(DEFAULT_ROOT_FOLDER,
				date + "/" + uuid + "/" + resourceName);
		return resourceWrapperOptional
				.map(resourceWrapper -> handleRequest(httpMethod, ifNoneMatchHeader, ifModifiedSinceHeader, resourceWrapper))
				.orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
	
	private ResponseEntity<Resource> handleRequest(HttpMethod httpMethod, Optional<String> ifNoneMatchHeader,
			Optional<Date> ifModifiedSinceHeader, ResourceWrapper resourceWrapper) {
		if (cachedOnClientSide(ifNoneMatchHeader, ifModifiedSinceHeader, resourceWrapper)) {
			return notModified(resourceWrapper);
		}
		return prepareResponse(httpMethod, resourceWrapper);
	}

	private boolean cachedOnClientSide(Optional<String> ifNoneMatchHeader, Optional<Date> ifModifiedSinceHeader,
			ResourceWrapper resourceWrapper) {
		boolean ifNoneMatch = ifNoneMatchHeader.map(headerData -> headerData.equals(resourceWrapper.getETag()))
				.orElse(false);
		boolean ifModifiedSince = ifModifiedSinceHeader
				.map(date -> !date.before(new Date(resourceWrapper.getLastModified()))).orElse(false);
		return ifNoneMatch || ifModifiedSince;
	}

	private ResponseEntity<Resource> notModified(ResourceWrapper resourceWrapper) {
		return createResponse(resourceWrapper, HttpStatus.NOT_MODIFIED, null);
	}

	private ResponseEntity<Resource> prepareResponse(HttpMethod httpMethod, ResourceWrapper resourceWrapper) {
		Resource resource = httpMethod == HttpMethod.GET ? createResource(resourceWrapper) : null;
		return createResponse(resourceWrapper, HttpStatus.OK, resource);
	}

	private Resource createResource(ResourceWrapper resourceWrapper) {
		RateLimiter rateLimiter = RateLimiter.create(BANDWIDTH_PER_CONNECTION);
		LimitedInputStream limitedInputStream = new LimitedInputStream(resourceWrapper.getInputStream(), rateLimiter);
		return new InputStreamResource(limitedInputStream);
	}

	private ResponseEntity<Resource> createResponse(ResourceWrapper resourceWrapper, HttpStatus httpStatus,
			Resource resource) {
		return ResponseEntity.status(httpStatus)
				.contentType(MediaType.parseMediaType(resourceWrapper.getContentType()))
				.eTag(resourceWrapper.getETag())
				.contentLength(resourceWrapper.getContentLength())
				.lastModified(resourceWrapper.getLastModified())
				.body(resource);
	}
}
