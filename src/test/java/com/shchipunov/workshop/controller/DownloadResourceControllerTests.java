package com.shchipunov.workshop.controller;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.springframework.http.HttpHeaders.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.shchipunov.component.ResourceStorage;
import com.shchipunov.component.ResourceWrapper;
import com.shchipunov.controller.DownloadResourceController;
import com.shchipunov.utility.DigestUtility;

/**
 * @author Shchipunov Stanislav
 */
@RunWith(SpringRunner.class)
@WebMvcTest(DownloadResourceController.class)
public class DownloadResourceControllerTests {

	private static final String PATH_TO_RESOURCE = "20160821/fcb1edde-bbb8-43fa-9cd9-ba18314537fb/test.txt";
	
	private static final String DEFAULT_ROOT_FOLDER = new File(File.separator).getAbsolutePath();
	private static final String RESOURCE_NAME = "test.txt";
	private static final String RESOURCE_CONTENT = "test";

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ResourceStorage resourceStorage;

	@Test
	public void shouldShow200() throws Exception {
		given(resourceStorage.findOne(DEFAULT_ROOT_FOLDER, PATH_TO_RESOURCE)).willReturn(resourceWrapper());

		mockMvc
				.perform(get("/download/" + PATH_TO_RESOURCE))
				.andExpect(status().isOk())
				.andExpect(content().string(RESOURCE_CONTENT));
	}
	
	@Test
	public void shouldShowNotFound() throws Exception {
		given(resourceStorage.findOne(DEFAULT_ROOT_FOLDER, PATH_TO_RESOURCE)).willReturn(Optional.ofNullable(null));

		mockMvc
				.perform(get("/download/" + PATH_TO_RESOURCE))
				.andExpect(status().isNotFound())
				.andExpect(content().bytes(new byte[0]));
	}
	
	@Test
	public void shouldShowNotModified() throws Exception {
		given(resourceStorage.findOne(DEFAULT_ROOT_FOLDER, PATH_TO_RESOURCE)).willReturn(resourceWrapper());

		mockMvc
				.perform(get("/download/" + PATH_TO_RESOURCE).header(IF_NONE_MATCH, resourceWrapper().get().getETag()))
				.andExpect(status().isNotModified())
				.andExpect(header().string(ETAG, resourceWrapper().get().getETag()));
	}

	private Optional<ResourceWrapper> resourceWrapper() {
		ResourceWrapper resourceWrapper = new ResourceWrapper() {

			@Override
			public String getOriginalName() {
				return RESOURCE_NAME;
			}

			@Override
			public long getLastModified() {
				return new Date().getTime();
			}

			@Override
			public InputStream getInputStream() {
				return new ByteArrayInputStream(RESOURCE_CONTENT.getBytes());
			}

			@Override
			public String getETag() {
				return "\"" + DigestUtility.hash(RESOURCE_CONTENT.getBytes(), "SHA-512") + "\"";
			}

			@Override
			public String getContentType() {
				return "text/plain";
			}

			@Override
			public long getContentLength() {
				return RESOURCE_CONTENT.length();
			}
		};
		return Optional.of(resourceWrapper);
	}
}
